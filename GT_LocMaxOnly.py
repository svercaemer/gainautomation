
import GT_CompareValues as CV
import GT_GainDifToFit as GDtF
import numpy as np
import matplotlib.pyplot as plt
import os
import ROOT
import sys

def makeGainRelList(gainRelFName, rootFName):
  folderCont = os.listdir('.')  
  if gainRelFName not in folderCont:
    CV.printRelations(rootFName, gainRelFName, False)
  
  gainRelList = []
  gainRelFile = open(gainRelFName, "r")
  for line in gainRelFile:
    sline = line.split()
    gainRelList.append(sline)
  gainRelFile.close()
  
  return gainRelList

def countValidPoints(GD):
  nPoints = 0
  xt,yt = ROOT.Double(0.1), ROOT.Double(0.1)

  gdKeys = GD.keys()
  for gdk in gdKeys:
    nChans = len(GD[gdk])
    for iChan in range(nChans):
      nEntries = GD[gdk][iChan].GetN()
      for iEntry in xrange(nEntries):
        GD[gdk][iChan].GetPoint(iEntry, xt, yt)
        x = float(xt)
        y = float(yt)
        if x != 0 and y != 0:
          nPoints += 1
  return nPoints
        

def printSiPMChars(fName, keys, gainRelList, order, HV):
  # get the board number
  bNum = 1
  if '_b0_' in fName:
    bNum = 0
    
  # get the TGraphs
  GraphDict = CV.getSelectionTGraphs(fName, keys, gainRelList, HV)
  
  # remove the outliers
  print "Board %i, %.1f V High voltage:" %(bNum, HV) 
  print "Channel\ta \tb \tc \ttrim bd"
  for chan in range(8):
    GL = []
    for key in keys:
      GL.append(GraphDict[key][chan])    
     
    # remove the outliers
    while GDtF.removeOutlier(GL):
      pass    
    
    # fit the combined local maxima spectra
    shortKeyList = ["gPM","gRMm"]
    # shortKeyList = ["PoormanMaxima", "ResidualsMaxima_LocMax"]
    bdt = -1
    a = 0 # zeroth order trim
    b = 0 # first order trim
    c = 0 # second order trim
    ff = CV.TGraphFitter([GraphDict["gPM"][chan],\
                           GraphDict["gRMm"][chan]], order)
    '''
    ff = CV.TGraphFitter([GraphDict["PoormanMaxima"][chan],\
                           GraphDict["ResidualsMaxima_LocMax"][chan]])
    '''
    if order == 1:  # process a linear fit
      try:
        a = ff.GetParameter(0)
        b = ff.GetParameter(1)
        bdt = -1.*a/b
      except ZeroDivisionError:
        pass
    else:  # process a quadratic fit
      try:
        a = ff.GetParameter(0)
        b = ff.GetParameter(1)
        c = ff.GetParameter(2)
        if b*b - 4*a*c > 0:
          bdt = (-1.*b - np.sqrt(b*b - 4*a*c))/(2*c)
      except ZeroDivisionError:
        pass
    mesg = "%i \t %.2f \t %.2f \t %.2f \t %.2f"\
           %(chan, a, b, c, bdt)
    print mesg
    
  CV.plotGainDict(GraphDict, bNum, HV, "TwoKeyFit_o%i" %(order), order, ["gPM", "gRMm"])
  
  
if __name__ == '__main__':
  fNameBasis = "/home/simon/SoLid/P1/SafFolder/saffron2_161206_LocMaxGain/gainOut_"
  fName0 = "%sb0_15.root" %(fNameBasis)
  fName1 = "%sb1_15.root" %(fNameBasis)
  
  # make the gain relations list
  gainRelFName = "gainRelations.txt"
  gainRelList = makeGainRelList(gainRelFName, fName0)
    
  # select a smaller list
  keys = ["gPA", "gPM", "gPI",\
          "gRAm", "gRMm", "gRIm",\
          "gRAt", "gRMt", "gRIt"]
  keys = ["gPA", "gPM", "gRAm", "gRMm"]
  '''
  keys = ['PoormanAmplitude', 'PoormanMaxima', 'PoormanIntegral',\
          'ResidualsAmplitude_LocMax', 'ResidualsMaxima_LocMax', 'ResidualsIntegral_LocMax'.\
          'ResidualsAmplitude_TSpec', 'ResidualsMaxima_TSpec', 'ResidualsIntegral_TSpec']
  keys = ['PoormanAmplitude', 'PoormanMaxima',\
          'ResidualsAmplitude_LocMax', 'ResidualsMaxima_LocMax']
  '''
  
  order = 1 # order of the polynomial to fit
  print "board 0"
  printSiPMChars(fName0, keys, gainRelList, order, 68.0)  
  printSiPMChars(fName0, keys, gainRelList, order, 68.5)  
  print "\nboard 1"
  printSiPMChars(fName1, keys, gainRelList, order, 68.0)
  printSiPMChars(fName1, keys, gainRelList, order, 68.5)