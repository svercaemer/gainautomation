#import GT_makeGainPlots as MGP
import GT_CompareValues as CV
import numpy as np
import matplotlib.pyplot as plt
import os
import ROOT
import sys

def rotateTGraph(G, a, b):
  nEntries = G.GetN()
  Gn = ROOT.TGraph(nEntries) # new TGraph
  xt,yt = ROOT.Double(0.1), ROOT.Double(0.1)
  for iEntry in xrange(nEntries):
    G.GetPoint(iEntry, xt, yt)
    x = float(xt)
    y = float(yt)
    Gn.SetPoint(iEntry, x, y - (a + b*x))
      
  return Gn

def getColor(key):
  if key == "gPA":  # new key: "PoormanAmplitude"
    return 1
  if key == "gPM":  # new key: "PoormanMaxima"
    return 632
  if key == "gRAm": # new key: "ResidualsAmplitude_LocMax"
    return 600
  if key == "gRMm": # new key: "ResidualsMaxima_LocMax"
    return 416
  return 0

# checks a TGraph list for outliers,
# removes at most one point, returns true if it does
# returns false if no point is removed
# the most distant point gets removed if it's
# square distance is more ten times the reduced chi square
def removeOutlier(GL):
  if type(GL) != type([]):
    GL = [GL]
  
  ff = CV.TGraphFitter(GL) 
  a = ff.GetParameter(0)
  b = ff.GetParameter(1)
  redChiSq = ff.GetChisquare()
  try:
    redChiSq /= ff.GetNDF()
  except ZeroDivisionError:
    return False  
  
  gNumMax = -1
  pNumMax = -1
  maxSqD = -1
  gNum = 0
  xt,yt = ROOT.Double(0.1), ROOT.Double(0.1)
  for G in GL:
    nEntries = G.GetN()
    for iEntry in xrange(nEntries):
      G.GetPoint(iEntry, xt, yt)
      x = float(xt)
      y = float(yt)
      sqD = y - (a + x*b)
      sqD *= sqD
      if sqD > maxSqD:
        maxSqD = sqD
        gNumMax = gNum
        pNumMax = iEntry
    gNum += 1
  
  if maxSqD > 10*redChiSq:
    GL[gNumMax].RemovePoint(pNumMax)
    return True # removed a point, return True
  # nothing got removed, return False
  return False
    

def makeGainCorPlot(fName, keys, HV, gainRelList):
  # get the board number
  bNum = 1
  if '_b0_' in fName:
    bNum = 0
    
  # get the TGraphs
  gd = CV.getSelectionTGraphs(fName, keys, gainRelList, HV)
  
  # make the plots
  nChan = len(gd[keys[0]])
  cName = "canv_b%i" %(bNum)
  c1 = ROOT.TCanvas(cName,cName,1000,500)
  col680 = 1
  col685 = 797
  for i in range(nChan):
    # set up the canvas
    c1.DrawFrame(-0.5, -5, 3.5, 5,\
      "SChannel %i (board %i), gain determination, 68.0V HV;" %(i, bNum) +\
      "Trim Voltage (V);" + "Distance to linear fit (ADC/PA)")
    leg = ROOT.TLegend(0.65, 0.1, 0.9, 0.1*(len(keys)+1))
    
    GL = []
    for key in keys:
      GL.append(gd[key][i])
      
    # remove the outliers
    while removeOutlier(GL):
      nPoints = 0
      for G in GL:
        nPoints += G.GetN()
      print 'removed 1 point, %i remaining' %(nPoints)
      pass
      
    # get the linear fits
    f0 = CV.TGraphFitter(GL) 
    a0 = f0.GetParameter(0)
    b0 = f0.GetParameter(1)
    newGraphList = []
    for key in keys:
      # get the new graph, draw it right away
      newGraphList.append(rotateTGraph(gd[key][i], a0, b0))      
      newGraphList[-1].SetMarkerColor(getColor(key))
      newGraphList[-1].SetMarkerStyle(CV.getMarker(key))
      newGraphList[-1].SetMarkerSize(3)
      newGraphList[-1].Draw("same P")      
      leg.AddEntry(newGraphList[-1],CV.makeLabel(key),"p")
    
    leg.Draw() 
    c1.Print("pcp/gainCor_b%i_c%i_v%.1f.png" %(bNum, i, HV),"png")
    
    for G in newGraphList:
      G.Delete()
    del newGraphList[:]

if __name__ == '__main__':
  fNameBasis = "/home/simon/SoLid/P1/SafFolder/saffron2_161206_LocMaxGain/gainOut_"
  fName0 = "%sb0_15.root" %(fNameBasis)
  fName1 = "%sb1_15.root" %(fNameBasis)
  #makeCompPlots(fName, False)
  
  # make the gain relations list
  folderCont = os.listdir('.')
  gainRelFName = "gainRelations.txt"
  if gainRelFName not in folderCont:
    printRelations(fName0, gainRelFName, False)
  
  gainRelList = []
  gainRelFile = open(gainRelFName, "r")
  for line in gainRelFile:
    sline = line.split()
    gainRelList.append(sline)
    
  # select a smaller list
  keys = ["gPA", "gPM", "gPI",\
          "gRAm", "gRMm", "gRIm",\
          "gRAt", "gRMt", "gRIt"]
  keys = ["gPA", "gPM", "gRAm", "gRMm"]
  '''
  keys = ['PoormanAmplitude', 'PoormanMaxima', 'PoormanIntegral',\
          'ResidualsAmplitude_LocMax', 'ResidualsMaxima_LocMax', 'ResidualsIntegral_LocMax'.\
          'ResidualsAmplitude_TSpec', 'ResidualsMaxima_TSpec', 'ResidualsIntegral_TSpec']
  keys = ['PoormanAmplitude', 'PoormanMaxima',\
          'ResidualsAmplitude_LocMax', 'ResidualsMaxima_LocMax']
  '''
  
  makeGainCorPlot(fName0, keys, 68.0, gainRelList)  
  #makeGainCorPlot(fName1, keys, gainRelList)