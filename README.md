# README #

### What is this repository for? ###

* Partially automate the process of SiPM callibration
* Saffron still has to be run individually

### Running Saffron to use these files ###

To use these files, the raw output of the test bench has to be processed. This is done using saffron with the SGainToolsTest enabled.
This algorithm currently lives in the feat/iss45/vs branch but the pull request has been accepted. This means it could move to dev soon.

The SGainToolsTest algorithm calculates the gain in 9 different ways and stores the results in a separate root file
(the name of which has to be specified in the options file, along with the trim and the high voltage).
Saffron has to be run over every setting individually, this produces a single root file (the settings are in the name, so they won't be overwritten)
per setting. When all raw root files have been processed by saffron, all the output files have to be hadded into a single file.
This single file can then be used for the 3 files that are here (12/01/2017).

### GT_CompareValues ###

This is the first of 3 python files. It has a number of important functions:

* getSelectionTGraphs(fName, keys, gainRelList, HV): this function reads in a file (fName) and for every entry checks if it is the correct high voltage (HV) and if it is a valid point by comparing all measures listed in keys. This checking happens by looking if the ratio of the different gain determinations fall within an acceptable ratio. This ratio is determined in the printRelations function, also in this file. All valid points are then stored in a dictionary. The dictionary has the measure as key and a list as item. This list contains 8 TGraphs, one for each channel. This number is set in line 197 [of this commit](https://bitbucket.org/svercaemer/gainautomation/commits/d9b06743361eb7428863acbb99b71ca633f77045?at=master), all other places use the length of the list.

* printRelations: this function loads a file, makes a histogram for every possible relation, fills it and fits a gaussian to it. the mean and width of that gaussian are then written to a provided file name. There is a function in GT_LocMaxOnly that reads this file and makes the list that is needed.

* TGraphFitter: This is probably the working horse of this program. It takes a list of TGraphs, merges these and performs a first or second order polynomial fit. It then returns the fitted function. An if statement is provided to limit the range of the quadratic term to negative values in a 2nd order fit. This is currently blocked by 'if False and order==2:'. It can be made active by removing 'False and' from the code. 

* plotGainDict(gd, bNum, HV, nameBasis, order, keys): plotter to plot the gain Dictionary (gd). It requires the board number (bNum) and high voltage (HV) for the naming of the plots. Also required is "namebasis", pretty self explanatory; order: the order of the fit that has to be plotted and keys: what keys have to be plotted (you don't necessarily want to plot all the keys)

### GT_GainDifToFit ###

This is a smaller file that has less useful functions in it, only the 'removeOutlier' function is really of intrest.

* removeOutlier: this function takes a graph list, merges these graphs, performs a linear fit and then removes the file with the largest contribution to the Chi Square if that contribution is larger than 10*chi^2/NDF. It returns true if a point got removed and false if no points got removed. That's why the removeOutlier function is mostly found in a while loop. The parameter 10 was found from a quick look at the chi squared contributions of the fits done on the high stat voltage scan data (taken 11, 12 and 13 of january 2017)

### GT_LocMaxOnly ###

It was found that from all measures, the gain determined on the local maxima spectra by the poor man method and the residuals method using the local maximum peak finder were the best. That explains the name of this file, it is also pretty short as it is mainly calling functions in the other two files. It is also the file to use.

* makeGainRelList(gainRelFName, rootFName): this makes the list with relations between the different gain determination methods. It checks if the provided relations file (gainRelFName) exists and creates it if not. It then reads this file and loads it into a list that can be read by getSelectionTGraphs.

* printSiPMChars(fName, keys, gainRelList, order, HV): this function takes a root file name (fName), the keys that have to be used in getSelectionTGraphs (keys), the order of the fit (order) and the high voltage (HV). It then prints per channel parameters a, b and c (0th, 1st and 2nd order; 2nd order is zero in case of linear) and the trim of the breakdown voltage. Inside the program, the keys that are used to make theses fits get reduced to only those mentioned earlier.


### General remarks ###

* this program is written with 8 channels in mind. Changing it to 64 only requires one adaptation in GT_CompareValues.

* the program is written for a version of the code before the current one. The changes that will have to be made are only in the flags. I've added the new flags in comment in the code. It could however always be that I forgot it in some place. The change can be seen [in this commit](https://bitbucket.org/solidexperiment/saffron2/commits/dc668d9f9d01cd29dfa52a3ce8c89f441774ec6b?at=dev), you can fill in from there.

* I (simon) Will be cube wrapping from 27/2 to 3/3. Those days, I will only be able to answer at night. I'm on holidays from 06/02 to 22/02 (the end of the collaboration meeting), I won't be able to answer any question during that period.
