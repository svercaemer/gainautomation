import GT_GainDifToFit as GDtF
#import GT_makeGainPlots as MGP
import numpy as np
import matplotlib.pyplot as plt
import os
import ROOT
import sys

# list of keys that are in the root file
# they are required to sit here as they are used in getCompHistos
keys = ["gPA", "gPM", "gPI",\
        "gRAm", "gRMm", "gRIm",\
        "gRAt", "gRMt", "gRIt"]
'''
# keys for the updated version of the SGainToolsTest algorithm
# they are in the same place, so this is also the conversion in case I missed a spot
keys = ['PoormanAmplitude', 'PoormanMaxima', 'PoormanIntegral',\
        'ResidualsAmplitude_LocMax', 'ResidualsMaxima_LocMax', 'ResidualsIntegral_LocMax'.\
        'ResidualsAmplitude_TSpec', 'ResidualsMaxima_TSpec', 'ResidualsIntegral_TSpec']
'''

def makeLabel(key):
  l = "#splitline{"
  if "P" in key:     # new key: "Poorman"
    l += "Poor Man"
  if "R" in key:     # new key: "Residuals"
    l += "Residuals"
  if "A" in key:     # new key: "Amplitude"
    l += " Amplitude}"
  if "I" in key:     # new key: "Integral"
    l += " Integral (x0.2)}"
  if "M" in key:     # new key: "Maxima"
    l += " Loc. Max.}"
  if "m" in key:     # new key: "LocMax"
    l += "{loc. max. peak finder}"
  elif "t" in key:   # new key: "TSpec"
    l += "{TSpectrum peak finder}"
  else:
    l += "{}"
  return l

def getHistoRange(key1, key2):
  # this keeps working with the updated keys
  nI = 0
  if 'I' in key1:
    nI += 1
  if 'I' in key2:
    nI += 2
    
  if nI == 0 or nI == 3:
    return 0, 2
  elif nI == 1:
    return 5, 10
  elif nI == 2:
    return 0, 1
  else:
    sys.exit()
    
def derivative(histo, binNum):
  nBins = histo.GetNbinsX()
  if binNum < 3 or binNum > nBins - 3:
    return 0
  dir5p = histo.GetBinContent(binNum - 2) -\
          8*histo.GetBinContent(binNum - 1) +\
          8*histo.GetBinContent(binNum + 1) -\
          histo.GetBinContent(binNum + 2)
  return dir5p/12
    
def getFitRange(histo, binNum):
  d = 1e3
  bl = binNum
  while d > 0:
    bl -= 1
    d = derivative(histo, bl)
  bh = binNum
  
  d = -1e3
  while d < 0:
    bh += 1
    d = derivative(histo, bh)
    
  return histo.GetXaxis().GetBinCenter(bl),\
         histo.GetXaxis().GetBinCenter(bh)

def getCompHistos(fName, beSmart=True):
  rf = ROOT.TFile(fName, "read")
  t = rf.Get("gainTree")
  nEntries = t.GetEntries()
  histosList = []
  global keys
  nKeys = len(keys)
  iMax = nKeys - 1
  if not beSmart:
    iMax = nKeys
  for i in range(iMax):
    jMin = i
    if not beSmart:
      jMin = 0
    for j in range(jMin, nKeys):
      if i == j and beSmart:
        continue
      rl, rh = getHistoRange(keys[i],keys[j])
      histosList.append(ROOT.TH1D("%s:%s" %(keys[i],keys[j]),\
                                  "%s:%s" %(keys[i],keys[j]),100,rl,rh))
      histosList[-1].SetDirectory(0)
  
  for iEntry in xrange(nEntries):
    t.GetEntry(iEntry)
    histoCounter = 0
    iMax = nKeys - 1
    if not beSmart:
      iMax = nKeys
    for i in range(iMax):
      jMin = i
      if not beSmart:
        jMin = 0
      for j in range(jMin, nKeys):
        if i == j and beSmart:
          continue
        v1 = t.GetLeaf(keys[i]).GetValue(0)
        v2 = t.GetLeaf(keys[j]).GetValue(0)
        histosList[histoCounter].Fill(v1/v2)
        histoCounter += 1
              
  rf.Delete() 
  return histosList

def fitHist(histo):
  maxBin = histo.GetMaximumBin()
  maxBinPos = histo.GetXaxis().GetBinCenter(maxBin)
  maxBinCont = histo.GetBinContent(maxBin)
  rl, rh = getFitRange(histo, maxBin)
  fG = ROOT.TF1("fitter","gaus",rl,rh)
  fG.SetParameter(0, maxBinCont)
  fG.SetParameter(1, maxBinPos)
  fG.SetParLimits(1, rl, rh)
  fG.SetParameter(2, histo.GetRMS())
  fG.SetParLimits(2, 0, 1.5*histo.GetRMS())
  histo.Fit(fG,"rq0")
  return fG

def getMarker(key):
  if key == "gPA":    # new key: "PoormanAmplitude"
    return 24 
  elif key == "gPM":  # new key: "PoormanMaxima"
    return 25
  elif key == "gRAm": # new key: "ResidualsAmplitude_LocMax"
    return 26 
  elif key == "gRMm": # new key: "ResidualsMaxima_LocMax"
    return 27 
  else:
    return 28 

def makeCompPlots(fName, beSmart=True):
  histosList = getCompHistos(fName, beSmart)        
  c1 = ROOT.TCanvas("c1")
  ROOT.gStyle.SetOptFit(1)
  for histo in histosList:
    histo.Draw()
    fG = fitHist(histo)
    fG.Draw("same")
    c1.Print("%s.png" %(histo.GetName()), "png")
    fG.Delete()
    histo.Delete()
    
def printRelations(infName, outfName, beSmart=True):
  histosList = getCompHistos(infName, beSmart)
  outF = open(outfName,"w")
  for histo in histosList:
    fG = fitHist(histo)
    sName = histo.GetName().split(':')
    k1 = sName[0] # key 1
    k2 = sName[1] # key 2
    gMean = fG.GetParameter(1)
    gWidth = fG.GetParameter(2)
    line = "%s %s %f %f\n" %(k1, k2, gMean, gWidth)
    outF.write(line)
    fG.Delete()
    histo.Delete()
  outF.close()
  
def getMeanWidth(k1, k2, gainRelList):
  for pair in gainRelList:
    if k1 == pair[0] and k2 == pair[1]:
      return float(pair[2]), float(pair[3])
    continue
  print "Pair %s %s not found" %(k1,k2)
  sys.exit()
  
def getSelectionTGraphs(fName, keys, gainRelList, HV):  
  rf = ROOT.TFile(fName, "read")
  t = rf.Get("gainTree")
  nEntries = t.GetEntries()
  nAcc = 0
  totEnt= 0
  
  nChan = 8 # assumes 8 channels, have to change it here to 64 if needed
  
  # set up TGraphs
  gd = {}
  for key in keys:
    gd[key] = []
    for i in range(nChan):
      gd[key].append(ROOT.TGraph(nEntries))
  
  for iEntry in xrange(nEntries):    
    t.GetEntry(iEntry)
    # check if good high voltage
    if t.GetLeaf("HV").GetValue(0) != HV: # new key: "HighVolt"
      continue
    
    # check if all entries are within relations
    failedKeyLib = {}
    for key in keys:
      failedKeyLib[key] = 0
    for k1 in keys:
      for k2 in keys:
        totEnt += 1      
        mean, width = getMeanWidth(k1, k2, gainRelList)
        v1 = t.GetLeaf(k1).GetValue(0)
        v2 = t.GetLeaf(k2).GetValue(0)
        r = 0
        try:
          r = v1/v2
        except ZeroDivisionError:
          continue
        if (r > mean - 3*width and r < mean + 3*width):
          pass
        else:
          failedKeyLib[k1] += 1
          failedKeyLib[k2] += 1
      
    # fill TGraphs
    for key in keys:
      if failedKeyLib[key] < 3:
        TV = t.GetLeaf("TV").GetValue(0)      # new key: "TrimVolt"
        HV = t.GetLeaf("HV").GetValue(0)      # new key: "HighVolt"
        cn = int(t.GetLeaf("cn").GetValue(0)) # new key: "ChanVolt"
        gain = t.GetLeaf(key).GetValue(0)
        if gain > 60:
          continue
        gd[key][cn].SetPoint(iEntry, TV, gain)
        nAcc += 1
  
  # remove the empty points
  xt, yt = ROOT.Double(0.0), ROOT.Double(0.0)
  for key in keys:
    for iChan in xrange(nChan):
      iEntry = 0
      while iEntry < gd[key][iChan].GetN():
        gd[key][iChan].GetPoint(iEntry, xt, yt)
        if xt == 0 and yt == 0:
          gd[key][iChan].RemovePoint(iEntry)
        else:
          iEntry += 1
  
  t.Delete()
  rf.Delete()
  return gd

# fit a polynomial of order order to a tgraph (all the graphs provided in Graph list GL).
def TGraphFitter(GL, order=1):
  if type(GL) == type(ROOT.TGraph(1)):
    GL = [GL]
    
  # find the number of points and the range to be fitted
  rangeLow = 1e6
  rangeHigh = -1e6
  xt,yt = ROOT.Double(0.1), ROOT.Double(0.1)
  nPoints = 0
  for G in GL: # for graph in praph list
    nPoints += G.GetN()
  
  # fill the fit graph
  fitGraph = ROOT.TGraph(nPoints)
  pointNum = 0
  for G in GL:
    for iEntry in xrange(G.GetN()):
      G.GetPoint(iEntry, xt, yt)
      x = float(xt)
      y = float(yt)
      fitGraph.SetPoint(pointNum, x, y)
      pointNum += 1
      if rangeHigh < x:
        rangeHigh = x
      if rangeLow > x:
        rangeLow = x
          
  # create the function to be fitted     
  fitFunc = ROOT.TF1("fitFunc","pol%i" %(order),\
                     rangeLow, rangeHigh)
  if nPoints == 0:
    return fitFunc
  
  if False and order == 2:
    fitFunc.SetParameter(2, -0.1)
    fitFunc.SetParLimits(2, -10, 0)
  
  # fit the function
  fitGraph.Fit(fitFunc, "RQB")
  
  return fitFunc

def plotFitResults(canv, px, py, col, HV, func):
  canv.cd() # plot on the proper canvas
  chisq = func.GetChisquare()
  nDoF = func.GetNDF()
  txt = ROOT.TLatex()
  txt.SetTextColor(col)
  nPars = func.GetNpar()
  formula = "G = %.1f" %(func.GetParameter(0))
  for parNum in range(1, nPars):
    val = func.GetParameter(parNum)
    if val >= 0:
      formula += " + %.1f" %(val)
    else:
      formula += " - %.1f" %(-1.*val)
    formula += " #times TV"
    if parNum > 1:
      formula += "^{%i}" %(parNum)
  txt.DrawLatex(px, py, "HV = %.1f V " %(HV) +\
                "%s (#chi^{2}/NDF = %.2f/%i)" %(formula, chisq, nDoF))
  
  rl,rh = ROOT.Double(0.0), ROOT.Double(2.0)  
  func.GetRange(rl, rh)
  rl -= 0.25
  rh += 0.25
  func.SetRange(rl, rh)
  func.SetLineColor(col)
  func.SetLineWidth(3)
  func.Draw("same")
  
 
def plotGainDict(gd, bNum, HV, nameBasis="GainPlots", order=1, keys=0):
  if keys == 0:
    keys = gd.keys()
  
  # make per channel plots
  nChan = len(gd[keys[0]])
  cName = "canv_b%i" %(bNum)
  c1 = ROOT.TCanvas(cName,cName,1000,500)
  col680 = 1
  col685 = 797
  for i in range(nChan):
    c1.DrawFrame(-0.5,0,3.5,70,\
      "SChannel %i (board %i), gain determination;" %(i, bNum) +\
      "Trim Voltage (V);" + "Gain (ADC/PA)")
    leg = ROOT.TLegend(0.65, 0.1, 0.9, 0.1*(len(keys)+1))
    GL = []
    for key in keys:
      # draw the 68V data
      gd[key][i].SetMarkerColor(col680)
      gd[key][i].SetMarkerStyle(getMarker(key))
      gd[key][i].SetMarkerSize(3)
      c1.cd()
      gd[key][i].Draw("same P")
      leg.AddEntry(gd[key][i], makeLabel(key),"p")
      
      # fill the graph lists
      GL.append(gd[key][i])
      
    leg.Draw()    
    
    # remove the outliers
    while GDtF.removeOutlier(GL):
      pass # function returns true if an element got removed
    
    ff = TGraphFitter(GL, order)
    plotFitResults(c1, 0, 60, col680, HV, ff)
    
    c1.Update()
    c1.Print("pcp/%s_b%i_c%i_v%.1f.png" %(nameBasis, bNum, i, HV),"png")
    
def makeSelectionPlots(fName, keys, HV, gainRelList):
  # get the board number
  bNum = 1
  if '_b0_' in fName:
    bNum = 0
    
  # get the TGraphs
  gd = getSelectionTGraphs(fName, keys, gainRelList, HV)
  plotGainDict(gd, bNum, HV, "gainExt")
    
    
if __name__ == '__main__':
  fNameBasis = "/home/simon/SoLid/P1/SafFolder/saffron2_161206_LocMaxGain/gainOut_"
  fName0 = "%sb0_15.root" %(fNameBasis)
  fName1 = "%sb1_15.root" %(fNameBasis)
  #makeCompPlots(fName, False)
  
  # make the gain relations list
  folderCont = os.listdir('.')
  gainRelFName = "gainRelations.txt"
  if gainRelFName not in folderCont:
    printRelations(fName0, gainRelFName, False)
  
  gainRelList = []
  gainRelFile = open(gainRelFName, "r")
  for line in gainRelFile:
    sline = line.split()
    gainRelList.append(sline)
    
  # select a smaller list
  keys = ["gPA", "gPM", "gPI",\
          "gRAm", "gRMm", "gRIm",\
          "gRAt", "gRMt", "gRIt"]
  keys = ["gPA", "gPM", "gRAm", "gRMm"]
  '''
  keys = ['PoormanAmplitude', 'PoormanMaxima', 'PoormanIntegral',\
          'ResidualsAmplitude_LocMax', 'ResidualsMaxima_LocMax', 'ResidualsIntegral_LocMax'.\
          'ResidualsAmplitude_TSpec', 'ResidualsMaxima_TSpec', 'ResidualsIntegral_TSpec']
  keys = ['PoormanAmplitude', 'PoormanMaxima',\
          'ResidualsAmplitude_LocMax', 'ResidualsMaxima_LocMax']
  '''
  
  makeSelectionPlots(fName0, keys, 68.0, gainRelList) 
  makeSelectionPlots(fName0, keys, 68.5, gainRelList)  
  makeSelectionPlots(fName1, keys, 68.0, gainRelList)
  makeSelectionPlots(fName1, keys, 68.5, gainRelList)
        
      
  
    
      
  
